# [Laravel SQL Binder](https://laravel-sql-binder.netlify.app)

## Description
Laravel SQL Binder is a tool that helps you extract your SQL queries from the Laravel Query Log Output. This tool is designed to simplify the process of debugging and analyzing SQL queries by providing a clean, easy-to-use interface to view and manage your query logs.

## Installation

To install the Laravel SQL Binder, follow these steps:

```bash
git clone https://github.com/yourusername/laravel-sql-binder.git
cd laravel-sql-binder
npm install
npm run dev
```

## License
Distributed under the MIT License. See LICENSE for more information.