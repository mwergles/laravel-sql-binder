import { createApp } from 'vue'
import './assets/index.css'
import 'highlight.js/styles/base16/monokai.min.css'
import highlight from 'highlight.js/lib/core'
import sql from 'highlight.js/lib/languages/sql'
import App from './App.vue'

highlight.registerLanguage('sql', sql)

createApp(App)
  .mount('#app')
