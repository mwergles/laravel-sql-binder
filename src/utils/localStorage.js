const NAMESPACE = 'lsb'

export function saveToLocalStorage (key, value) {
  try {
    localStorage.setItem(`${NAMESPACE}:${key}`, JSON.stringify(value))
  } catch (error) {
    // localStorage full or disabled
  }
}

export function getFromLocalStorage (key) {
  try {
    return JSON.parse(localStorage.getItem(`${NAMESPACE}:${key}`)) ?? {}
  } catch (error) {
    return {}
  }
}
