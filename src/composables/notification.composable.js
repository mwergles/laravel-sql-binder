import { ref } from 'vue'

const messages = ref([])

const DEFAULT_TIMEOUT = 3000

export default function useNotification () {
  const showNotification = (text, type = 'success', timeout = DEFAULT_TIMEOUT) => {
    const id = Math.random().toString(16).slice(2)
    const message = { id, text, type }
    messages.value.push(message)

    message.timeoutId = setTimeout(() => {
      messages.value = messages.value.filter(message => message.id !== id)
    }, timeout)
  }

  const removeMessage = (id) => {
    const message = messages.value.find(message => message.id === id)
    if (message) {
      clearTimeout(message.timeoutId)
      messages.value = messages.value.filter(message => message.id !== id)
    }
  }

  const clearMessages = () => {
    messages.value.forEach(message => {
      clearTimeout(message.timeoutId)
    })
    messages.value = []
  }

  return {
    showNotification,
    removeMessage,
    clearMessages,
    messages,
  }
}
