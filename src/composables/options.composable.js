import { saveToLocalStorage, getFromLocalStorage } from '@/utils/localStorage'

export default function useOptions () {
  const options = getFromLocalStorage('options')

  const setOptions = (newOptions) => {
    const mergedOptions = {
      ...options,
      ...newOptions,
    }

    saveToLocalStorage('options', mergedOptions)
  }

  return {
    options,
    setOptions,
  }
}
